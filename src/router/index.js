import { createRouter,createWebHistory} from 'vue-router'
// import Home from '../components/indashboard';
import Dashboard from '../components/dashboard';
import DashboardInfo from '../components/dashboardinfo';
import chart from '../components/chart';
import inside1 from '../components/Lamtakong';
import inside2 from '../components/Sikhio';
import inside3 from '../components/Khamtalayso';
import inside4 from '../components/Sungnoen';
import setting from '../components/setting';
import chemical from '../components/chemical';
import notification from '../components/notification';
import insidetank from '../components/insidetank1';
import login from '../components/login';
import  NotFound from '../components/notPage'
const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/info',
    name: 'info',
    component: DashboardInfo
  },
  {
    path: '/chart',
    name: 'chart',
    component: chart
  },
  {
    path: '/Lamtakong',
    name: 'inside1',
    component: inside1
  },
  {
    path: '/Sikhio',
    name: 'inside2',
    component: inside2
  },
  {
    path: '/Khamtalayso',
    name: 'inside3',
    component: inside3
  },
  {
    path: '/Sungnoen',
    name: 'inside4',
    component: inside4
  }
  ,
  {
    path: '/Silt',
    name: 'insidetank',
    component: insidetank
  },
  {
    path: '/notification',
    name: 'notification',
    component: notification
  },
  {
    path: '/setting',
    name: 'setting',
    component: setting
  },
  {
    path: '/login',
    name: 'login',
    component: login
  },{
    path: '/chemical',
    name: 'chemical',
    component: chemical
  },
  {
    path: "/:catchAll(.*)",
    component: NotFound,
  },
  
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
