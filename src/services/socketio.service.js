import { io } from 'socket.io-client';
class SocketioService { 
//   payload;
  socket;
  constructor() {
    
  }
 
  setupSocketConnection() {  
    
    this.socket = io(process.env.VUE_APP_SOCKET_ENDPOINT);  
    
    // this.socket.emit('my message', 'Hello there from Vue.');
    this.socket.on('data', async(data) => {
      // console.log("data");  
      // console.log(data);
        // this.payload = 123;
         await this.readData(data);
      });
      
  }

  readData(data2){
    
    // console.log(data2)
    this.payload = data2
  }


  disconnect() {
    if (this.socket) {
        this.socket.disconnect();
    }
}
}

export default new SocketioService();